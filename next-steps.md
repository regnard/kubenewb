---
description: Ready to go the next level?
---

# Next Steps

The good news: You are done with KubeNewb! This site offers a basic introduction to Kubernetes concepts and intended to demystify what it is and lower the barrier to further learning.&#x20;

The bad news: There's still a long way to go.&#x20;

But don't fret, there's a good path ahead, and hopefully this site's contents have provided some clarity.

I can recommend two steps from here: to go deep with learning more about the technical aspects of Kubernetes and cloud native applications. The other path is to take courses from cloud service providers and get a taste of the different flavours of how Kubernetes is implemented and used in the industry.

### Deeper Kubernetes path:

The [Kubernetes and Cloud Native Essentials](https://training.linuxfoundation.org/training/kubernetes-and-cloud-native-essentials-lfs250/) course (LF250) is a great intermediate point for people who would like to learn more about the architecture and other adjacent topics to Kubernetes, such as GitOps.

### Wider cloud service provider path:

You can't go wrong if you focus on the top three cloud service providers (AWS, Azure, and Google Cloud) and get familiarized with their offerings.

* AWS - [https://aws.amazon.com/kubernetes/](https://aws.amazon.com/kubernetes/)
* Azure - [https://azure.microsoft.com/en-us/products/kubernetes-service/](https://azure.microsoft.com/en-us/products/kubernetes-service/)
* Google Cloud - [https://cloud.google.com/kubernetes-engine](https://cloud.google.com/kubernetes-engine)

### Let's connect!

If you found this site helpful or if you need any clarification, please send me a [LinkedIn invite](https://www.linkedin.com/in/regnard). Just mention that you found me from KubeNewb.
