---
description: A simple definition of everyday people
---

# What is Kubernetes?

Kubernetes is software that automatically manages applications organized together in groupings called "containers."

Think of containers as software bundled together in a package that makes it possible for the software to be used and consumed. Do you remember the game cartridges of old gaming consoles like the NES? The container in this analogy is the physical cartridge that houses the software embedded in the chip, and Kubernetes is like the console itself.

Extending that analogy a bit: Kubernetes will make it possible to house all the cartridges you like inside the console instead of you physically changing them. Furthermore, when one cartridge gets corrupted, Kubernetes will be the one blowing air on it and try to get it back up to its working state.



