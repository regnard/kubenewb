---
description: An invitation to learn something about Kubernetes
---

# Welcome

You're probably here because you're a KubeNewb (aka, a newbie to [Kubernetes](https://kubernetes.io/)).&#x20;

I wholeheartedly welcome you to this free online course I made because I thought there's a lack of easy-to-follow introductory courses on Kubernetes.

If you're a non-engineering person or an enthusiast who wants to understand the technology landscape better, this course is for you. Moreover, if you're a manager who wishes to know what your team is doing, this is a good course for you as well.

My main goal here is to use simple terms and use real-life analogies so that you will go away with a basic understanding, and you can explain what Kubernetes is to your friends. Heck, you can even use it at dinner parties if you like!

&#x20;\---

Regnard Raquedan

[https://regnard.raquedan.com](https://regnard.raquedan.com)
