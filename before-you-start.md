---
description: Course prerequisites and foundations
---

# Before you start

Knowing these things can go a long way in meeting the course's learning objectives. Included are good resources to learn more about these building blocks.

## Cloud Computing

To better appreciate Kubernetes, it is a prerequisite to be familiar with cloud computing.&#x20;

Cloud computing makes a variety of technology services available online.&#x20;

One good way to imagine it is you take pieces of your computer-- the processor, the storage, and the software, for example-- and you can connect to them and use them on-demand. Organizations often deploy Kubernetes in cloud-based environments.

Cloud service providers like [AWS](http://aws.amazon.com/), [Azure](https://azure.microsoft.com/), and [GCP](https://cloud.google.com/) are your best bets to learn how "The Cloud" works.

Here are some useful bite-sized resources for further reading on cloud computing:

* [https://cyber.gc.ca/en/guidance/what-cloud-computing-itsap50110](https://cyber.gc.ca/en/guidance/what-cloud-computing-itsap50110)
* [https://www.pcmag.com/how-to/what-is-cloud-computing](https://www.pcmag.com/how-to/what-is-cloud-computing)
* [https://www.techopedia.com/definition/2/cloud-computing](https://www.techopedia.com/definition/2/cloud-computing)

## Microservices

Microservices is a topic worth noting because Kubernetes lends itself very well to this type of approach to designing software applications.

Traditionally, developers coded software together in a common programming language and packaged together as one, also known as the "Monolithic" approach to building software.&#x20;

The contemporary approach has been to break up this bundled design into smaller "services" for ease of developing and releasing, and independence from the other parts of the software. Improved computer networking and the remarkable leap in computing power has made this approach viable in the past decade.

There's a [free course on Microservices](https://www.freecodecamp.org/news/microservices-architecture-for-humans/) from freeCodeCamp that makes it easy for non-tech folks to get a foundational appreciation of Microservices.

## Containers

Containers and Kubernetes often go hand in hand because Kubernetes manages resources attached to containers. Containers are like packages that have the software and all the parts the software needs to run properly.&#x20;

One good analogy I have for this is a burger. The burger has all its ingredients like the patty, the lettuce, the tomato, the ketchup, and the pickle held together by a wrapper. Imagine that the wrapper is the container and the software and its components are the ingredients.

Here's a good [introductory video lecture on Containers](https://www.coursera.org/lecture/linux-cloud-devops/containers-f6PPi).
