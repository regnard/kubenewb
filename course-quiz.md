---
description: A short assessment of what you learned
---

# Course Quiz

1. What is Kubernetes primarily used for? \
   a. Hosting websites \
   b. Managing containerized applications \
   c. Flying kites \
   d. Sending emails
2. What does the term "container" refer to in the context of Kubernetes? \
   a. A shipping container \
   b. A software bundle with dependencies \
   c. A cooking pot \
   d. A storage unit
3. What is one benefit of using Kubernetes? \
   a. Automatically managing application deployments \
   b. Enhancing video game performance \
   c. Improving cooking skills \
   d. Increasing internet speed
4. Kubernetes is often associated with which software architecture pattern? \
   a. Monolithic \
   b. Microservices \
   c. Procedural \
   d. Object-oriented
5. Which cloud provider is NOT among the top three Kubernetes service providers? \
   a. AWS \
   b. Azure \
   c. Google Cloud \
   d. MySpace
6. In a Kubernetes cluster, what is the role of the master node? \
   a. Storing data \
   b. Managing the worker nodes \
   c. Running user applications \
   d. Generating power
7. What is the smallest unit in Kubernetes for deploying an application? \
   a. Cluster \
   b. Node \
   c. Pod \
   d. Service
8. What does the term "scaling" mean in the context of Kubernetes? \
   a. Changing the size of an image \
   b. Climbing a mountain \
   c. Adjusting the number of instances of an application \
   d. Measuring weight
9. Which of the following is NOT a Kubernetes object? \
   a. Deployment \
   b. ReplicaSet \
   c. Ingress \
   d. Espresso
10. Which Kubernetes component is responsible for container runtime? \
    a. kube-apiserver \
    b. etcd \
    c. kubelet \
    d. kube-scheduler

**Answers**: 1. b, 2. b, 3. a, 4. b, 5. d, 6. b, 7. c, 8. c, 9. d, 10. c
