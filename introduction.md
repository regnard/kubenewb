---
description: Answering the What, the Why, and the How of this course
---

# Introduction

## Background

When I first heard about Kubernetes, I thought, "Oh great, another buzzword in tech!"

I resisted the initial urge to learn about this new shiny tech even though I was already in the tech industry for over 10 years. I was already used to the emergence of new terms, from "Web 2.0" to "Cloud Computing." I didn't feel like being an [early adopter](https://en.wikipedia.org/wiki/Early\_adopter) to Kubernetes.

But by 2020, it felt like Kubernetes was everywhere.

Blog articles, LinkedIn posts, and online videos filling my daily content stream were picking up more and more items about Kubernetes. I imagine if there was no COVID-19 pandemic that year, Kubernetes would be the topics of conversation in tech community events and gatherings.

Still, I resisted the urge to do a deep dive, mainly out of fear.

It also didn't help that the "introductory" content on Kubernetes didn't feel and read like foundational material at all. Many of the first content on this topic felt like a white paper meant to establish the mystique of this foreign-sounding technology (spoiler alert: Kubernetes is Greek for "helmsman").

It was only in 2021 after feeling some pandemic fatigue when I finally had the motivation to try to pick up Kubernetes and begin my slow burn to get to know it.

After several months and reading articles and watching tutorials, I had my lightbulb moment after going through a Cloud Academy online course (which I was able to access as part of the [AWS Community Builders](https://aws.amazon.com/developer/community/community-builders/) program).&#x20;

And soon after that "Now I get it!" moment, I had one more realization: there's has to be an easier pathway for people to learn about Kubernetes, especially non-engineering folks.

## The One Learning Objective

The course has only one objective: After going through this course, you are expected to explain and describe what Kubernetes is and list at least one reason how it can benefit you and your project or organization.

I want to make it as simple as possible using common English terms that you would encounter when reading the news or your favorite TV review website.

At the end of the course, there is a short quiz to assess what you've learned. (Hopefully you do learn something.)&#x20;
